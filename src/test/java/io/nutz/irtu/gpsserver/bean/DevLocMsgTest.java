package io.nutz.irtu.gpsserver.bean;

import static org.junit.Assert.*;

import org.junit.Test;
import org.nutz.json.Json;

public class DevLocMsgTest {
    
    private final static String mHexStr = "0123456789ABCDEF"; 

    @Test
    public void testFromByteArray() {
        String hex = "aa015d82339d437db3ed0df3898a000d00b6051f04";
        byte[] buf = hexStr2Str(hex);
        DevLocMsg msg = DevLocMsg.from(buf);
        assertNotNull(msg);
        System.out.println(Json.toJson(msg));
        assertNotEquals(msg.lat, 23.0f);
    }


    public static byte[] hexStr2Str(String hexStr){  
        hexStr = hexStr.toUpperCase();
        char[] hexs = hexStr.toCharArray();  
        byte[] bytes = new byte[hexStr.length() / 2];  
        int iTmp = 0x00;;  
 
        for (int i = 0; i < bytes.length; i++){  
            iTmp = mHexStr.indexOf(hexs[2 * i]) << 4;  
            iTmp |= mHexStr.indexOf(hexs[2 * i + 1]);  
            bytes[i] = (byte) (iTmp & 0xFF);  
        }  
        return bytes;  
    }
}
