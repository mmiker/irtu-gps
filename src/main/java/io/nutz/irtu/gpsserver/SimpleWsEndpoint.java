package io.nutz.irtu.gpsserver;

import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.mvc.websocket.AbstractWsEndpoint;
import org.nutz.plugins.mvc.websocket.NutWsConfigurator;
import org.nutz.plugins.mvc.websocket.WsHandler;
import org.nutz.plugins.mvc.websocket.handler.SimpleWsHandler;

/**
 * 页面websocket入口
 *
 */
@ServerEndpoint(value = "/websocket", configurator = NutWsConfigurator.class)
@IocBean
public class SimpleWsEndpoint extends AbstractWsEndpoint {

    public WsHandler createHandler(Session session, EndpointConfig config) {
        return new SimpleWsHandler(""); // 使用自带SimpleWsHandler就可以了,自带房间功能
    }
}
